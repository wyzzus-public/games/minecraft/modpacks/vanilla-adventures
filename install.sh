echo Running in $MODE mode...

MODS_DIR=$CLIENT_DIR

if [[ $MODE == "Server" ]]; then
	MODS_DIR=$SERVER_DIR
fi

RESULT_MODS=()

BLACKLIST=()

while IFS= read -r line; do
	BLACKLIST+=("$line")
done < client_only.txt

BLACKLIST_CLIENT=()

while IFS= read -r line; do
        BLACKLIST_SERVER+=("$line")
done < server_only.txt

for entry in ./mods/*
do
	PREFIX="./mods/"
	MOD_NAME=${entry/$PREFIX/""}

	NEED_INSTALL=True

	if [[ $MODE == "Server" ]]; then
		
		if [[ ${BLACKLIST[@]} =~ $MOD_NAME ]]; then
			echo Excluding $MOD_NAME ...
			NEED_INSTALL=False
        	fi        	
	fi

	if [[ $MODE == "Client" ]]; then

                if [[ ${BLACKLIST_SERVER[@]} =~ $MOD_NAME ]]; then
                        echo Excluding $MOD_NAME ...
                        NEED_INSTALL=False
                fi
        fi

	if [[ $NEED_INSTALL == "True" ]]; then
		RESULT_MODS+=($MOD_NAME)
        fi
done

echo $MODS_DIR

rm -rf "$MODS_DIR"*

for value in "${RESULT_MODS[@]}"
do
	cp -R "./mods/$value" "$MODS_DIR"
done

TOTAL_MODS=${#RESULT_MODS[@]}

echo Installed $TOTAL_MODS mods into ${MODS_DIR}
